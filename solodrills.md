---
layout: default
title: Solodrills
---

# Corona-Solodrills

18.03.2020

Michael Lammer

## Grundlagen

### Körperhaltung

* Während der gesamten Sequenz bleibt der Körper zentral gespannt, aber nicht verkrampft.
* Alle Schwertaktionen beginnen ihre Ausführung in der Bewegung der Hüfte.
* Die Hände schließen sich gleichmäßig und unverkrampft um den Schwertgriff. Der kleine Finger und der Ringfinger üben die meiste Kraft aus, der Zeigefinger dient der Steuerung der Bewegungen.
* Die Handgelenke werden nicht abgewinkelt. Die jeweilige Hand bleibt in einer Ebene mit dem Unterarm.

### Kräftedreieck

* Das Schwert wird so gegriffen, dass beide Arme beim erreichen der maximalen Reichweite gestreckt sind. 
* Dazu muss gegebenenfalls etwas enger gegriffen werden (d.h. bei langen Gehilzen liegen nun beide Hände am Griff). 
* In der Hut Langort bilden die beiden Arme und der Schultergürtel ein Dreieck, dessen Spitze die Hände bzw. der Schwertgriff sind.

### Schritte und Schwertaktionen

* Die vorwärtsgerichtete Schrittarbeit erfolgt in der Regel kurz bevor das Schwert seine maximale Reichweite erreicht. 
* Die rückwärtsgerichtete SChrittarbeit startet zusammen mit der Schwertaktion.

## Einfache Übungen

* **Oberhaue** mit langer Schneide
  * diagonaler Hau von oben nach unten
  * aus Hut vom Tag über Langort in Wechsel (andere Seite)
  * Ganzer Schritt kurz vor dem Erreichen von Langort
  * Die Sequenz endet, wenn die Arme den Unterleib/Torso erreichen und der Hau so gestoppt wird.

* **Unterhaue** mit kurzer Schneide
  * diagonaler Hau von unten nach oben
  * Aus Wechsel über Langort in Hut vom Tag (andere Seite)
  * Ganzer Schritt kurz vor dem Erreichen von Langort

* **Oberstiche**
  * Stich mit den Händen hoch
  * Aus Ochs in oberes Hängen (gleiche Seite)
  * Halber Schritt kurz vor dem Erreichen von oberem Hängen
* **Unterstiche**
  * Stich mit den Händen tief
  * Aus Pflug in unters Hängen (gleiche Seite)
  * Halber Schritt kurz vor dem Erreichen von unterem Hängen
* **Langer Stich** 
  * Stich mit den Händen tief
  * Aus Pflug in Langort (zentral)
  * Ganzer Schritt kurz vor dem Erreichen von Langort

## Zusammengesetzte Übungen

* **Oberhau und Stich**
  * diagonaler Hau von oben, dann Stich mit den Händen tief
  * Aus Hut vom Tag über unteres Hängen (andere Seite) in Langort
  * Wechselschritt aus der Linie kurz vor dem Erreichen von unterem Hängen, halber Schritt kurz vor dem Erreichen von Langort
* **Inverser Wechselhau**
  * diagonaler Hau von unten, dann  diagonaler Hau von oben
  * Aus Wechsel über Langort in Hut vom Tag (andere Seite), von dort wieder in Langort
  * Wechselschritt aus der Linie kurz vor dem Erreichen von Langort von unten, ganzer Schritt kurz vor dem Erreichen von Langort von oben

## Komplexe Sequenzen

* **Ochs-Oberhau-Kombination**
  * Hutenwechsel, dann Stich mit den Händen hoch, Ort abkippen und Hau von oben nach unten zur anderen Seite
  * Aus Hut vom Tag in Ochs (andere Seite); in oberes Hängen, dann wieder in Ochs und wieder in oberes Hängen (immer selbe Seite), über Hangetort in Hut vom Tag über Kopf; über Langort in Nebenhut (andere Seite)
  * Ganzer Schritt beim Hutenwechsel, halber Schritt kurz vor dem Erreichen von oberem Hängen (zweimal), Wechselschritt aus der Linie beim Durchlaufen von Hangetort

* **Oberhau-Zwerchhau-Kombination**
  * diagonaler Hau von oben nach unten, dann Zwerch-Unterhau wieder nach oben
  * Aus Ochs über Hangetort und Langort in Wechsel (andere Seite), dann über unteres Hängen mit Zwerch-Unterhau über Hangetort wieder in Ochs (wieder ursprüngliche Seite)
  * Ganzer Schritt kurz vor dem Erreichen von Langort von oben, ganzer Schritt kurz vor dem Erreichen von Hangetort von unten

